package main

import (
	"google.golang.org/grpc"
	"log"
	model "gitlab.com/elena.shebaldenkova/readFileClientBegin/model"
	"context"
	"fmt"
	"github.com/smartystreets/scanners/csv"
	"os"
	"io"
	"github.com/spf13/viper"
	"path/filepath"
)

/*const(
	addr = "localhost:8080"
	pathToFile = "C:\\GoPath\\src\\gitlab.com\\elena.shebaldenkova\\readFileClientBegin\\phonebook100000000.csv"
)*/


func main() {
	viper.SetConfigName("config") // no need to include file extension
	path, err := filepath.Abs("../config/")
	fmt.Println("Config path:", path)
	if err != nil {
		fmt.Println("Config file not found...")
	}
	viper.AddConfigPath(path)
	// set the path of your config file
	error := viper.ReadInConfig()
	if error != nil {
		fmt.Println("Config file not found...")
	} else {
		addr := viper.GetString("client.addr")

		conn, err := grpc.Dial(addr, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("grpc.Dial() Error: %v", err)
		}
		defer conn.Close()

		client := model.NewRecServiceClient(conn)
		msg := readCsvAndSend(client)
		fmt.Println(msg)
	}
}
type ContactScanner struct {
	*csv.Scanner
}

func NewContactScanner(reader io.Reader) *ContactScanner {
	inner := csv.NewScanner(reader)
	inner.Scan() // skip the header!
	return &ContactScanner{Scanner: inner}
}

func (this *ContactScanner) Record() (*model.Record) {
	fields := this.Scanner.Record()
	return &model.Record{
			Phonenumber: fields[0],
			Name:        fields[1],
			Addres:      fields[2],
		}
	}



func readCsvAndSend(client model.RecServiceClient) string {
	pathToFile := viper.GetString("client.pathToFile")
	file, err := os.OpenFile(pathToFile, os.O_RDONLY, os.ModePerm)
	if err != nil {
		log.Fatalf("os.OpenFile() Error: %v", err)
	}

	stream, err := client.SendRecordsFromFileToBolt(context.Background())
	if err != nil {
		log.Fatalf("client.SendRecordsFromFileToBolt() Error: %v", err)
	}

	scanner := NewContactScanner(file)
	request := &model.Record{}
	fmt.Println("Start streaming data to server")

	for scanner.Scan() {
		request = scanner.Record()
		if err := stream.Send(request); err != nil {
			log.Fatalf("stream.Send(request) Error: %v", err)
		}
	}

	if err = scanner.Error(); err != nil {
		log.Fatalf("scanner.Error() Error: %v", err)
	}

	response, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("stream.CloseAndRecv() Error: %v", err)
	}
	return response.Message
}
